<?php

namespace Civigo\Distro;

class Version {
  const CIVIGO_DISTRO_VERSION = "{{version}}";

  public static function get() {
    return self::CIVIGO_DISTRO_VERSION;
  }

}
